<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('engine_types', function (Blueprint $table) {
            $table->id();
            $table->enum('engine_form', ['inline, W, V, Boxer, Flat, Rotary']);
            $table->string('engine_volume');
            $table->string('cylinder_count');
            $table->enum('aspiration_type', ['Naturally, TurboCharged, SuperCharged']);
            $table->boolean('approved')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('engine_type');
    }
};
