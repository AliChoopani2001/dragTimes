<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transmission_types', function (Blueprint $table) {
            $table->id();
            $table->enum('trans_type', ['AT, MT'])->nullable('MT');
//            $table->enum('gearbox_type', ['AT, MT']);
            $table->enum('AT_type', ['Dual Clutch, Auto, Single Clutch'])->nullable();
            $table->enum('MT_type', ['STD, Dogbox, Sequential'])->nullable();
            $table->boolean('approved')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transmission_type');
    }
};
