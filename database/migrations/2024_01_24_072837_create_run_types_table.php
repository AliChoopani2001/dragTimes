<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('run_types', function (Blueprint $table) {
            $table->id();
            $table->enum('run_length', [1/8, 1/4, 1/2, 1])->default(1/4);
            $table->timestamp('reaction_time');
            $table->timestamp('60_feet');
            $table->boolean('approved')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('run_type');
    }
};
