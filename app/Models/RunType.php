<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RunType extends Model
{
    use HasFactory;

    protected $fillable = [
        '',
    ];
}
