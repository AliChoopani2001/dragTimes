<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'images',
        'banner',
        'seo_title',
        'seo_keyword',
        'seo_description',
        'approved'
    ];
}
