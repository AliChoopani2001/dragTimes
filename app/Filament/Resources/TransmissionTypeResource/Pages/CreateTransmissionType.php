<?php

namespace App\Filament\Resources\TransmissionTypeResource\Pages;

use App\Filament\Resources\TransmissionTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTransmissionType extends CreateRecord
{
    protected static string $resource = TransmissionTypeResource::class;
}
