<?php

namespace App\Filament\Resources\EngineTypeResource\Pages;

use App\Filament\Resources\EngineTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateEngineType extends CreateRecord
{
    protected static string $resource = EngineTypeResource::class;
}
