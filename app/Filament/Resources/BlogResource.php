<?php

namespace App\Filament\Resources;

use App\Filament\Resources\BlogResource\Pages;
use App\Filament\Resources\BlogResource\RelationManagers;
use App\Models\Blog;
use Faker\Provider\Text;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class BlogResource extends Resource
{
    protected static ?string $model = Blog::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')->required()->label('Title'),
                Forms\Components\Textarea::make('description')->required()->label('Description'),
                Forms\Components\FileUpload::make('images')->required()->label('Images'),
                Forms\Components\FileUpload::make('banner')->required()->label('Banner'),
                Forms\Components\TextInput::make('seo_title')->required()->label('Seo Title'),
                Forms\Components\TextInput::make('seo_keyword')->required()->label('Seo Keyword'),
                Forms\Components\Textarea::make('seo_description')->required()->label('Seo Description'),
                Forms\Components\Checkbox::make('approved')->default(1)->label('Approved')
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->label('ID'),
                Tables\Columns\TextColumn::make('title')->label('Title'),
                Tables\Columns\ImageColumn::make('banner')->label('Banner'),
                Tables\Columns\CheckboxColumn::make('approved')->label('Approved'),
                Tables\Columns\TextColumn::make('created_at')->label('Create')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBlogs::route('/'),
            'create' => Pages\CreateBlog::route('/create'),
            'edit' => Pages\EditBlog::route('/{record}/edit'),
        ];
    }
}
