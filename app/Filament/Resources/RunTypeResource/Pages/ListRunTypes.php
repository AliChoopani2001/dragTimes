<?php

namespace App\Filament\Resources\RunTypeResource\Pages;

use App\Filament\Resources\RunTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListRunTypes extends ListRecords
{
    protected static string $resource = RunTypeResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
