<?php

namespace App\Filament\Resources\RunTypeResource\Pages;

use App\Filament\Resources\RunTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateRunType extends CreateRecord
{
    protected static string $resource = RunTypeResource::class;
}
