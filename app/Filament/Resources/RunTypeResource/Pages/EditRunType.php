<?php

namespace App\Filament\Resources\RunTypeResource\Pages;

use App\Filament\Resources\RunTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditRunType extends EditRecord
{
    protected static string $resource = RunTypeResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
